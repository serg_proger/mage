<?php

class WD_CronTasks_Model_Observer
{
    /**
     * Run schedules in tables with status code "pending"
     */
    public function runSchedules()
    {

        $modelCron = Mage::getModel('crontasks/crontasks');
        $collection = $modelCron
                            ->getCollection()
                            ->addFieldToFilter('status', 'false');
        require_once(Mage::getBaseDir('lib').'/wd_pdf/index.php');
        foreach ($collection as $banners) {
            # code...
           if($banners->getStatus() == 'false')
           {

              $data['status'] = 'in progress';
              $model_set = $modelCron->load($banners->getId())->addData($data);
              try {
                        $model_set->setId($banners->getId())->save();
                       
                       
                    } catch (Exception $e){
                        echo $e->getMessage(); 
                }

              genpdf($banners->getCode(), 
                     $banners->getBanner_id(), 
                     $banners->getOrientation(), 
                     $banners->getSize(), 
                     $banners->getType(), 
                     $banners->getSku(), 
                     $banners->getQty(), 
                     $banners->getInc_id(),
                     $banners->getPrefix()
                     );
              $data['status'] = 'true';
              $model_set = $modelCron->load($banners->getId())->addData($data);
              try {
                        $model_set->setId($banners->getId())->save();
                       
                       
                    } catch (Exception $e){
                        echo $e->getMessage(); 
                }
           }
        }

        //Mage::dispatchEvent(); dispatch custom event if need
    }
}