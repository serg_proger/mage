<?php

class  WD_CronTasks_Model_Resource_Crontasks_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('crontasks/crontasks');
    }
}