<?php

class WD_CronTasks_Model_Crontasks
    extends Mage_Core_Model_Abstract
{

    const STATUS_CODE_DONE = 2;
    const STATUS_CODE_PENDING = 1;
    const STATUS_CODE_ERROR = 0;

    protected function _construct()
    {
        $this->_init('crontasks/crontasks');
    }
}