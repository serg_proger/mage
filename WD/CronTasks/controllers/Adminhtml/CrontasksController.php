<?php

class WD_CronTasks_Adminhtml_CrontasksController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Cron Tasks'));

        $this->loadLayout();
        $this->_setActiveMenu('cron_tasks');
        $this->_addBreadcrumb(Mage::helper('crontasks')->__('Cron Tasks'), Mage::helper('crontasks')->__('Cron Tasks'));
        $this->renderLayout();
    }

    public function newAction()
    {
        Mage::getSingleton('core/session')->addError(Mage::helper('crontasks')->__('This functionality not working'));
        $this->_redirectReferer();
    }

    public function editAction()
    {
        $this->_title($this->__('Edit entity'));

        $this->loadLayout();
        $this->_setActiveMenu('crontasks');
        $this->_addBreadcrumb(Mage::helper('crontasks')->__('Edit entity'), Mage::helper('crontasks')->__('Edit entity'));
        $this->renderLayout();
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id', false);

        try {
            Mage::getModel('crontasks/crontasks')->setId($id)->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('crontasks')->__('Entity successfully deleted'));

            return $this->_redirect('*/*/');
        } catch (Mage_Core_Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('Somethings went wrong'));
        }

        $this->_redirectReferer();
    }

    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        if (!empty($data)) {
            try {
                $model = Mage::getModel('crontasks/crontasks');
                if (array_key_exists('id', $data)) {
                    $model->load((int)$data['id']);
                }

                if(array_key_exists('status', $data)) {
                    $model->setData('status',(int)$data['status']);
                }

                $model->save();

                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('crontasks')->__('Entity successfully saved'));
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError($this->__('Somethings went wrong'));
            }
        }
        return $this->_redirect('*/*/');
    }
}