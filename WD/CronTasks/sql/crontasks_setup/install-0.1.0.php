<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('magekeeper_cron_tasks'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
    ), 'Code')
    ->addColumn('orientation', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable'  => false,
    ), 'Orientation')
    ->addColumn('size', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable'  => false,
    ), 'Size')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable'  => false,
    ), 'Type')
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable'  => false,
    ), 'Sku')
    ->addColumn('qty', Varien_Db_Ddl_Table::TYPE_INTEGER, 8, array(
        'nullable'  => false,
    ), 'Qty')
    ->addColumn('inc_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'nullable'  => false,
    ), 'Inc_id')
    ->addColumn('prefix', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'nullable'  => false,
    ), 'Prefix')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, 3, array(
        'nullable'  => false,
    ), 'Status')
;

$installer->getConnection()->createTable($table);

$installer->endSetup();