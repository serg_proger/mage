<?php

class WD_CronTasks_Block_Adminhtml_Crontasks
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        $this->_addButtonLabel = Mage::helper('crontasks')->__('Add New');

        $this->_blockGroup = 'crontasks';
        $this->_controller = 'adminhtml_crontasks';
        $this->_headerText = Mage::helper('crontasks')->__('Cron Tasks');
    }
}