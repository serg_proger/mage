<?php

class WD_Crontasks_Block_Adminhtml_Renderer_Status
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        return Mage::helper('crontasks')->convertStatusCode($row->getStatus());
    }
}