<?php
/**
 * @author Pavel Usachev <webcodekeeper@hotmail.com>
 * @copyright Copyright (c) 2016, Pavel Usachev
 */

class WD_CronTasks_Block_Adminhtml_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $item = Mage::registry('current_crontasks');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('edit_crontasks', array(
            'legend' => Mage::helper('crontasks')->__('Cron Task Details')
        ));

        if ($item && $item->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
                'required' => true
            ));
        }

        $fieldset->addField('code', 'text', array(
            'name' => 'code',
            'label' => Mage::helper('crontasks')->__('Code'),
            'maxlength' => '250',
            'disabled' => true,

        ));

        $fieldset->addField('status', 'select', array(
            'name' => 'status',
            'label' => Mage::helper('crontasks')->__('Status'),
            'value' => '1',
            'values' => Mage::helper('crontasks')-> getStatusCodes(),
            'required' => true,
        ));



        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $form->setEnctype('multipart/form-data');
        $form->setValues($item->getData());

        $this->setForm($form);
    }
}