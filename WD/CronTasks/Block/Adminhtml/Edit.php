<?php

class WD_CronTasks_Block_Adminhtml_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * @var string
     */
    protected $_objectId = 'id';

    protected function _construct()
    {
        $this->_blockGroup = 'crontasks';
        $this->_mode = 'edit';
        $this->_controller = 'adminhtml';

        $itemId = (int)$this->getRequest()->getParam($this->_objectId);
        $item = Mage::getModel('crontasks/crontasks')->load($itemId);

        Mage::register('current_crontasks', $item);
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        $item = Mage::registry('current_crontasks');
        if ($item && $item->getId()) {
            return Mage::helper('crontasks')->__("Edit Build Willys '%s'", $this->escapeHtml($item->getSku()));
        } else {
            return Mage::helper('crontasks')->__("Add new item");
        }
    }
}