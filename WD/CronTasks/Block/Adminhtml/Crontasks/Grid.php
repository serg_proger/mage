<?php

class WD_CronTasks_Block_Adminhtml_Crontasks_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _construct()
    {
        $this->setId('crontasksGrid');
        $this->_controller = 'adminhtml_crontasks';
        $this->setUseAjax(true);

        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('crontasks/crontasks')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return MageKeeper_CronTasks_Block_Adminhtml_Crontasks_Grid
     *
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'        => Mage::helper('crontasks')->__('ID'),
            'align'         => 'right',
            'width'         => '20px',
            'filter_index'  => 'id',
            'index'         => 'id'
        ));

        $this->addColumn('code', array(
            'header'        => Mage::helper('crontasks')->__('Code'),
            'align'         => 'left',
            'filter_index'  => 'code',
            'index'         => 'code',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('orientation', array(
            'header'        => Mage::helper('crontasks')->__('Orientation'),
            'align'         => 'center',
            'filter_index'  => 'orientation',
            'index'         => 'orientation',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('size', array(
            'header'        => Mage::helper('crontasks')->__('Size'),
            'align'         => 'center',
            'filter_index'  => 'size',
            'index'         => 'size',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true
        ));

        $this->addColumn('type', array(
            'header'        => Mage::helper('crontasks')->__('Type'),
            'align'         => 'left',
            'filter_index'  => 'type',
            'index'         => 'type',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('sku', array(
            'header'        => Mage::helper('crontasks')->__('Sku'),
            'align'         => 'left',
            'filter_index'  => 'sku',
            'index'         => 'sku',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('status', array(
            'header'        => Mage::helper('crontasks')->__('Status'),
            'align'         => 'center',
            'index'         => 'status',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
            'renderer'      => 'MageKeeper_CronTasks_Block_Adminhtml_Renderer_Status'
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('crontasks')->__('Action'),
            'width'     => '50px',
            'type'      => 'action',
            'getter'     => 'getId',
            'actions'   => array(
                array(
                    'caption' => Mage::helper('crontasks')->__('Edit'),
                    'url'     => array(
                        'base'=>'*/*/edit',
                    ),
                    'field'   => 'id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'id',
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $entity
     *
     * @return string
     */
    public function getRowUrl($entity)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $entity->getId(),
        ));
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}