<?php

class WD_CronTasks_Helper_Data
    extends Mage_Core_Helper_Abstract
{

    /**
     * @var array
     */
    protected $_statusCodes = array();

    /**
     * MageKeeper_CronTasks_Helper_Data constructor.
     */
    public function __construct()
    {
        $this->_statusCodes = array(
            WD_CronTasks_Model_Crontasks::STATUS_CODE_PENDING   => $this->__('Pending'),
            WD_CronTasks_Model_Crontasks::STATUS_CODE_DONE      => $this->__('Done'),
            WD_CronTasks_Model_Crontasks::STATUS_CODE_ERROR     => $this->__('Error'),
        );
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) Mage::getStoreConfig('cron_tasks/general/enabled');
    }

    /**
     * @param $statusCode
     * @return string
     */
    public function convertStatusCode($statusCode)
    {
        if (array_key_exists((int) $statusCode, $this->_statusCodes)) {
            return $this->_statusCodes[(int) $statusCode];
        }

        return $this->__('Unknown');
    }

    /**
     * @return array
     */
    public function getStatusCodes()
    {
        return $this->_statusCodes;
    }
}
	 